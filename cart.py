from item import GroceryProduct


class Cart:
    """This is a container that acts as an inventory of shopping items"""

    def __init__(self, *args):
        """Create a new shopping cart
        :param args: Items that can go in the cart upon its creation
        :type args: <GroceryProduct object>
        """
        self._inventory = list(args)
        self._total = 0.0

    @property
    def inventory(self):
        """Get current inventory
        :return: Returns a list of all items in shopping cart
        :rtype: list[<GroceryProduct object>]"""
        return self._inventory

    @property
    def total(self):
        """Get total cost of all items in shopping cart
        :return: Returns a number
        :rtype: float
        """
        return self._total

    @total.setter
    def total(self, value):
        """Set the total cost of all items in shopping cart
        :param value: This is the total amount owed by customer at any given point. Floor at zero.
        :type value: float"""
        if value > 0:
            self._total = value
        else:
            self._total = 0

    def cart_total(self, payment_amt):
        """Calculates the total cost of all items in cart
        :param payment_amt: Payment amount so far
        :type payment_amt: float
        :return: Returns a total cost number minus any customer payments
        :rtype: float
        """
        item_subtotals = [GroceryProduct.item_total_cost(item) for item in self._inventory]
        if item_subtotals:
            return self._calculate_cart_total(item_subtotals) - payment_amt
        else:
            return 0.00

    def format_cart_total(self):
        """Formats the shopping cart total cost for display to user
        :return: Formatted string of the total
        :rtype: str
        """
        formatted_total = self.total
        return f'Total: ${formatted_total:.2f}'

    def view(self, sort_by=None):
        """Returns formatted list of all inventory items, optionally sorted by name or subtotal

        :param sort_by: Sorts items by name or subtotal, optional
        :type sort_by: str
        :return: self._inventory if it exists, otherwise string saying cart is empty
        :rtype: list[<item.GroceryProduct object>] or str
        """
        if not self._inventory:
            return f'The cart is empty.'
        else:
            items = []
            for item in self._inventory:
                items.append([item, item.item_total_cost()])
            if sort_by == 'name':
                # Reaches in and sorts lexicographically ascending by name
                items.sort(key=lambda x: x[0].name)
            elif sort_by == 'subtotal':
                # Reaches in and sorts value ascending by subtotal amount
                items.sort(key=lambda x: x[1])
        # Remember sort order
        self._inventory = [item[0] for item in items]

        return [f'[ {num + 1} ] - {repr(item)} | ${subtotal:.2f}' for num, (item, subtotal) in enumerate(items)]

    def update(self, operation, item=None):
        """Adds, removes an item, or empties the cart

        :param operation: "add", "remove", or "empty"
        :param item: a specific item object to add or remove, optional
        :type operation: str
        :type item: <item.GroceryProduct object>, optional
        :return: self._inventory if valid operation, or str otherwise
        :rtype: list[<item.GroceryProduct object>] or str
        :raise: ValueError if item for removal can't be found
        """
        # Add
        if item and operation == 'add':
            self._inventory.append(item)
            return self.view()
        # Remove
        elif item and operation == 'remove':
            try:
                self._inventory.remove(item)
                if self._inventory:
                    return self.view
                else:
                    return f'Your cart is empty.'
            except ValueError:
                raise ValueError('Unsuccessful in removing item from inventory.')
        # Empty list
        elif not item and operation == 'empty':
            try:
                del self._inventory[:]
                return f'Your cart is empty.'
            except:
                return f'Your cart is empty.'
        else:
            return f'Invalid command. Please try again'

    def find_item(self, item_id):
        """Find a specific GroceryProduct item in inventory

        :param item_id: the unique identifier for an item object. String of digits.
        :type item_id: str
        :return: The actual item object
        :rtype: <item.GroceryProduct object>
        :raise: Value Error if item_id isn't digits
        :raise: IndexError if list is empty or item id isn't in list
        """
        # Grab item from inventory by matching display id, which is one-indexed
        try:
            item_list = [item for item in self._inventory if self._inventory.index(item) == int(item_id) - 1]
            return item_list[0]
        except ValueError:
            raise ValueError('item_id must be composed entirely of digits')
        except IndexError:
            raise IndexError('That item id is not in the list, or the list is empty')

    @staticmethod
    def _calculate_cart_total(item_subtotals):
        """Returns sum of all item subtotals

        :param item_subtotals: All item subtotal prices
        :type item_subtotals: list[float]
        :return: Final cost to customer
        :rtype: float
        """
        return sum(item_subtotals)


if __name__ == '__main__':
    pass