import datetime


class Payment:
    """Handles payment collection and information storage"""

    def __init__(self):
        """Create new payment transaction"""
        self._first_name = None
        self._last_name = None
        self._cc_number = None
        self._expiration = None
        self._paid = 0

    @property
    def first_name(self):
        """Get customer first name

        return: Customer first name
        :rtype: str
        """
        return self._first_name

    @property
    def last_name(self):
        """ Get customer last name

        :return: Customer last name
        :rtype: str"""
        return self._last_name

    @property
    def cc_number(self):
        """Get obfuscated credit card number

        :return: Gets the credit card number, and replaces all but last four digits with Xs for display
        :rtype: str
        """
        if self._cc_number:
            return f'XXXXXXXXXXXX{self._cc_number[-4 : ]}'

    @property
    def expiration(self):
        """Get credit card expiration date

        :return: Expiration date
        :rtype: <datetime.date object>
        """
        return self._expiration

    @property
    def paid(self):
        """Get amount paid by customer across 1 or more cash transactions

        :return: Total amount paid by customer
        :rtype: float
        """
        return self._paid

    @first_name.setter
    def first_name(self, value):
        """Set customer first name

        :param value: Customer first name
        :type value: str
        :raise: ValueError if non-alphabetic
        """
        if value.isalpha():
            self._first_name = value
        else:
            raise ValueError('First name must be alphabetic')

    @last_name.setter
    def last_name(self, value):
        """Set customer last name

        :param value: Customer last name
        :type value: str
        :raise: ValueError if non-alphabetic
        """
        if value.isalpha():
            self._last_name = value
        else:
            raise ValueError('Last name must be alphabetic')

    @cc_number.setter
    def cc_number(self, value):
        """Sets credit card number for payment

        :param value: Credit card number
        :type value: str
        :raise: ValueError if non-numeric or < 16 digits
        """
        value = value.replace(' ', '')
        if not value.isdigit():
            raise ValueError('Credit card number must be all numbers')
        elif len(value) != 16:
            raise ValueError('Credit card must be 16 digits long')
        else:
            self._cc_number = value

    @expiration.setter
    def expiration(self, value):
        """Set expiration date on credit card

        :param value: Credit card expiration date
        :type value: str
        :raise: ValueError if expiration date not in MM/DD/YYYY format
        """
        try:
            value = tuple([int(i) for i in value.split('/')])
            month, day, year = value[0], value[1], value[2]
            expiration_date = datetime.date(year, month, day)
            print(f'datetime: {expiration_date}')
            if expiration_date < datetime.date.today():
                raise ValueError('It appears your card has expired. Please try another.')
            else:
                self._expiration = expiration_date
        except ValueError:
            raise ValueError('Please enter the expiration date in the specified format.')

    @paid.setter
    def paid(self, value):
        """Total amount customer paid across transactions for a given shopping cart

        :param value: Total payment amount
        :type value: str
        :raise: ValueError if non-numeric or less than zero
        """
        try:
            value = float(value)
            if value > 0:
                self._paid += value
            else:
                raise ValueError('Please provide a payment larger than zero.')
        except ValueError:
            raise ValueError('Please provide a number.')

    def pay_card(self):
        """Returns dictionary of customer credit card info

        :return: Returns all customer's credit card information, with credit card number obfuscated
        :rtype: dict
        """

        return {
            'first name': self.first_name,
            'last name': self.last_name,
            'card number': self.cc_number,
            'expiration': self.expiration
        }


if __name__ == '__main__':
    expiration('03/13/2020')