import unittest
from cart import Cart
from item import GroceryProduct


class TestCart(unittest.TestCase):
    def setUp(self):
        self.item1 = GroceryProduct()
        self.item1.name = 'apple'
        self.item1.mmt_unit = 'piece'
        self.item1.qty = '2'
        self.item1.price = '1.50'

        self.item2 = GroceryProduct()
        self.item2.name = 'zucchini'
        self.item2.mmt_unit = 'piece'
        self.item2.qty = '1'
        self.item2.price = '1.00'

        self.item3 = GroceryProduct()
        self.item3.name = 'steak'
        self.item3.mmt_unit = 'lb'
        self.item3.qty = '1.5'
        self.item3.price = '7.00'

        self.cart1 = Cart(self.item1, self.item2, self.item3)
        self.cart2 = Cart()
        self.cart3 = Cart(self.item1)

    def test_inventory(self):
        self.assertEqual(len(self.cart1.inventory), 3)
        self.assertIsInstance(self.cart1.inventory, list)
        self.assertIsInstance(self.cart2.inventory, list)

    def test_total(self):
        self.assertIsInstance(self.cart1.total, float)
        # Check setter floors at zero
        self.cart1.total = -1.0
        self.assertEqual(self.cart1.total, 0.0)
        self.cart1.total = 25.99
        self.assertEqual(self.cart1.total, 25.99)
        self.assertEqual(self.cart2.total, 0.0)

    def test_cart_total(self):
        self.assertIsInstance(self.item1, GroceryProduct)
        cart1_total = self.cart1.cart_total(0.00)
        self.assertEqual(cart1_total, 14.50)

    def test_format_cart_total(self):
        cart1_total = self.cart1.cart_total(0.00)
        self.cart1.total = cart1_total
        formatted_total = self.cart1.format_cart_total()
        self.assertEqual(formatted_total, 'Total: $14.50')

    def test_cart_view(self):
        no_inv = self.cart2.view()
        self.assertEqual(no_inv, 'The cart is empty.')
        no_sort = self.cart1.view()
        self.assertEqual(no_sort, [
            '[ 1 ] - apple: 2.0 piece @ $1.50 | $3.00',
            '[ 2 ] - zucchini: 1.0 piece @ $1.00 | $1.00',
            '[ 3 ] - steak: 1.5 lb @ $7.00 | $10.50'
        ])

        sub_sort = self.cart1.view(sort_by='subtotal')
        self.assertEqual(sub_sort, [
            '[ 1 ] - zucchini: 1.0 piece @ $1.00 | $1.00',
            '[ 2 ] - apple: 2.0 piece @ $1.50 | $3.00',
            '[ 3 ] - steak: 1.5 lb @ $7.00 | $10.50'
        ])

        name_sort = self.cart1.view(sort_by='name')
        self.assertEqual(name_sort, [
            '[ 1 ] - apple: 2.0 piece @ $1.50 | $3.00',
            '[ 2 ] - steak: 1.5 lb @ $7.00 | $10.50',
            '[ 3 ] - zucchini: 1.0 piece @ $1.00 | $1.00',
        ])

    def test_update(self):
        self.item4 = GroceryProduct()
        self.item4.name = 'added product'
        self.item4.mmt_unit = 'oz'
        self.item4.qty = '3'
        self.item4.price = '1.00'

        self.cart1.update('add', self.item4)
        self.assertEqual(len(self.cart1.inventory), 4)

        self.cart1.update('remove', self.item4)
        self.assertEqual(len(self.cart1.inventory), 3)

        # Trying to empty an empty cart
        msg = self.cart2.update('empty')
        self.assertEqual(msg, 'Your cart is empty.')
        # Trying to remove item from empty cart
        with self.assertRaises(Exception) as context:
            self.cart2.update('remove', self.item1)
            self.assertTrue('Unsuccessful in removing item from inventory.' in str(context.self.cart1.update))

        # Trying to remove item no longer in cart
        with self.assertRaises(Exception) as context:
            self.cart1.update('remove', self.item4)
            self.assertTrue('Unsuccessful in removing item from inventory.' in str(context.self.cart1.update))

        # Going from one item to none
        msg = self.cart3.update('remove', self.item1)
        self.assertEqual(msg, 'Your cart is empty.')

    def test_find_item(self):
        # Find the zucchini item which has a display id of 2 for user
        item = self.cart1.find_item('2')
        self.assertIsInstance(item, GroceryProduct)
        self.assertTrue(item.name == 'zucchini')

        with self.assertRaises(Exception) as context:
            self.cart1.find_item('50')
            self.assertTrue('That item id is not in the list, or the list is empty' in str(context.self.cart1.update))

        with self.assertRaises(Exception) as context:
            self.cart1.find_item('apple')
            self.assertTrue('item_id must be composed entirely of digits' in str(context.self.cart1.update))

    def test_calculate_cart_total(self):
        item_subtotals = [item.item_total_cost() for item in self.cart1.inventory]
        total = self.cart1._calculate_cart_total(item_subtotals)
        self.assertEqual(total, 14.50)


if __name__ == '__main__':
    unittest.main()