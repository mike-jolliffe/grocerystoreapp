class GroceryProduct:
    """This class creates individual shopping items."""

    def __init__(self):
        """Creates a new item"""
        self._id = id(self)
        self._name = None
        self._mmt_unit = None
        self._qty = None
        self._price = None

    def __repr__(self):
        return f"{self._name}: {self._qty} {self._mmt_unit} @ ${self._price:.2f}"

    @property
    def id(self):
        """Gets the unique item id"""
        return self._id

    @property
    def name(self):
        """Gets item name"""
        return self._name

    @property
    def mmt_unit(self):
        """Gets measurement units"""
        return self._mmt_unit

    @property
    def qty(self):
        """Gets item quantity"""
        return self._qty

    @property
    def price(self):
        """Gets item unit price"""
        return self._price

    @id.setter
    def id(self, value):
        """Sets item id

        :value id: Numeric identifier for an item
        :type id: str
        :raise: ValueError for non-numerical ids
        """
        if value.isdigit():
            self._id = value
        else:
            raise ValueError('id must be all numerical digits.')

    @name.setter
    def name(self, value):
        """Sets item name

        :param  value: The item's name
        :type value: str
        raise: ValueError for non-alphabetic chars
        """
        if not value:
            raise ValueError('Please enter a name.')
        for char in value:
            if char.isalpha() or char == ' ':
                self._name = value
            else:
                raise ValueError('Name must by alphabetic.')

    @mmt_unit.setter
    def mmt_unit(self, value):
        """Sets measurement units

        :param value: The units of measure for quantity
        :type value: str
        raise: ValueError for non-alphabetic chars
        """
        if value.isalpha():
            self._mmt_unit = value
        else:
            raise ValueError('Measurement units must be alphabetic.')

    @qty.setter
    def qty(self, value):
        """Sets quantity

        :param value: How much of an item in given units is transacted
        :type value: str
        :raise: ValueError if quantity is non-numeric
        """
        try:
            quantity = float(value)
            self._qty = quantity
        except:
            raise ValueError('Quantity must be a number')

    @price.setter
    def price(self, value):
        """Sets unit price

        :param value: The unit price of an item
        :type value: str
        :raise: ValueError if non-numeric
        """
        try:
            price = float(value)
            self._price = price
        except:
            raise ValueError('Price must be a number')

    def item_total_cost(self):
        """Returns the total item price for a given quantity
        :return: Total price for given item
        :rtype: float
        """
        return self._price * self._qty


if __name__ == '__main__':
    pass