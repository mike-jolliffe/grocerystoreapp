import os
import subprocess as sp
from cart import Cart
from payment import Payment


class Menu:
    """This executes user commands with dispatchers and contains print formatters for displaying results"""

    def __init__(self):
        """Instantiate the menu with dictionaries to dispatch user commands"""
        self._main_commands = {
            '1': ['Sort / View my Cart', Cart.view],
            '2': ['Add Item', Cart.update],
            '3': ['Remove Item', Cart.update],
            '4': ['Empty Cart', Cart.update],
            '5': ['Check Out', self.check_out],
            'e': ['Exit', exit]
        }

        self._checkout_commands = {
            '1': ['Pay with Cash'],
            '2': ['Pay with Card', Payment.pay_card],
            '3': ['Sort my Cart', Cart.view],
            '4': ['Go Back to Main Menu', Menu.display],
            'e': ['Exit', exit]
        }

    @property
    def main_commands(self):
        """Gets main menu command names

        return: Returns only command names for display, leaves functions behind
        rtype: dict
        """
        return {k: v[0] for k, v in self._main_commands.items()}

    @property
    def checkout_commands(self):
        """Gets checkout menu command names

        :return: Returns only command names for display, leaves functions behind
        :rtype: dict
        """
        return {k: v[0] for k, v in self._checkout_commands.items()}

    def display(self, data):
        """Used for formatting and printing a variety of shallow data structures to screen

        :param data: The data structure to be displayed.
        :type data: list, str, int/float, or dict"""

        def pretty_print(out_line, num_vars):
            """Makes an output line pretty and prints it

            :param out_line: A single element to print to terminal
            :param num_vars: The number of variables to be displayed
            :type out_line: tuple
            :type num_vars: int
            :rtype: None
            """
            # Print different data types different ways
            if num_vars == 1:
                print(f'{out_line[0]}')
            elif num_vars == 2:
                print(f'[ {out_line[0]} ] - {out_line[1]}\n')

        print('\n------------------------------------------\n')

        # Str, float, int are pretty_printed as num_vars of 1
        if isinstance(data, (str, float, int)):
            pretty_print((data,), 1)
        # Dicts pretty_print as num_vars of 2
        elif isinstance(data, dict):
            for key, val in data.items():
                pretty_print((key, val), 2)
        # Lists pretty_print as num_vars of 1
        elif isinstance(data, list):
            for value in data:
                pretty_print((value,), 1)

    def check_input(self, user_input, commands):
        """Checks user selected a valid command number for dispatcher dict

        :param user_input: A number representing the command to execute
        :param commands: Which set of commands to check input against
        :type user_input: str
        :type commands: dict
        :return: Will return True if user input is a key in available commands dict, else False
        :rtype:  bool
        """
        if user_input in commands.keys():
            return True
        else:
            return False

    def exec_command(self, command_list, command_num, *args):
        """Executes selected command in dispatcher

        :param command_list: Which list to use for command execution
        :param command_num: A user-specified action
        :type command_list: str
        :type command_num: str
        :return: Returns True if successfully executes
        :rtype: bool
        """
        if command_list == 'main':
            # Calling either main menu or checkout menu functions, invoking with any necessary positional args
            result = self._main_commands[command_num][1](*args)
        elif command_list == 'checkout':
            result = self._checkout_commands[command_num][1](*args)
        else:
            return f'Please provide "main" or "checkout" parameter to command_list parameter'
        return result

    def check_out(self):
        """Switches from main menu to checkout menu
        :return: Returns the dictionary of checkout menu commands
        :rtype: dict
        """
        return self.checkout_commands

    @staticmethod
    def clear():
        """Clear the console window"""
        try:
            if os.name == 'nt':
                sp.call('cls', shell=True)
            else:
                sp.call('clear', shell=True)
        except:
            pass


if __name__ == '__main__':
    pass