from cart import Cart
from item import GroceryProduct
from menu import Menu
from payment import Payment
import time


"""This file runs the grocery store app, collects user input, and manages most of the control flow."""

menu = Menu()
cart = Cart()
payment = Payment()

def main():
    # Switch used for changing which menus display to user
    screen, scr_type = menu.main_commands, 'main'

    menu.clear()
    while True:
        # Display the opening menu
        menu.display(cart.view())
        menu.display(screen)
        # User selects a command
        while True:
            user_input = input('Please select a number from the options above: ')
            # Check input is valid
            if menu.check_input(user_input, screen):
                # Collect any additional info required for command parameters
                params_pkg = build_params_pkg(scr_type, user_input)
                # Switch to main menu
                if params_pkg == 'main':
                    screen, scr_type = menu.main_commands, 'main'
                    menu.clear()
                # Switch to checkout menu
                elif params_pkg == 'checkout':
                    screen, scr_type = menu.checkout_commands, 'checkout'
                    menu.clear()
                    # Add shopping cart total to checkout menu
                    menu.display(cart.format_cart_total())
                # Execute an in-menu command, rather than switching menus
                else:
                    # Given a menu, user command number, and any further params for that command, execute it
                    menu.exec_command(scr_type, user_input, *params_pkg[user_input])
                    menu.clear()
                break
            else:
                print('Please choose a valid number')

def build_params_pkg(scr_type, user_input):
    """Grabs any required parameters from user, builds package to pass to function dispatcher dictionary

    :param scr_type: The menu from which commands are being executed
    :param user_input: Numeral representing user's chosen command
    :type scr_type: str
    :type user_input: str
    :return: Returns a dictionary with all the parameters required to run the chosen command, in the correct menu
    """
    params_pkg = None
    # MAIN MENU
    if scr_type == 'main':
        params_pkg = {
            '1': [cart],
            '2': [cart, 'add'],
            '3': [cart, 'remove'],
            '4': [cart, 'empty'],
            '5': [],
            'e': []
        }

        # "View / Sort my Cart"
        if user_input == '1':
            sort_by = input('Sort by [ name ], [ subtotal ], or hit Enter to skip: ')
            # Add chosen sorting functionality
            params_pkg['1'].append(sort_by)

        # "Add an Item"
        if user_input == '2':
            # Create new item and put it into the cart
            item = GroceryProduct()
            while True:
                try:
                    if not item.name:
                        item.name = input('Item name: ')
                    if not item.mmt_unit:
                        item.mmt_unit = input('Item measurement units: ')
                    if not item.qty:
                        item.qty = input('Item quantity: ')
                    if not item.price:
                        item.price = input('Item price: ')
                    break
                except ValueError as e:
                    print(e)
            # Pass the newly created item
            params_pkg['2'].append(item)

        # "Remove an Item"
        elif user_input == '3':
            menu.clear()
            menu.display(cart.view())
            print('\n\n')
            if cart.inventory:
                while True:
                    item_id = input('Which number item would you like to remove? ')
                    if not item_id.isdigit():
                        print('You must enter a valid number.')
                        continue
                    try:
                        item = cart.find_item(item_id)
                        params_pkg['3'].append(item)
                        break
                    except Exception as e:
                        print(e)
            else:
                menu.clear()
                print('\nCart is empty. Try adding items first.')
                time.sleep(2)

        # "Empty the Cart"
        elif user_input == '4':
            if not cart.inventory:
                menu.clear()
                print('\nCart is empty. Try adding items first.')
                time.sleep(2)

        # "Check Out"
        elif user_input == '5':
            cart.total = cart.cart_total(payment.paid)
            return 'checkout'
        elif user_input == 'e':
            menu.clear()
            print('\n\nThanks for shopping!\n\n')

    # CHECKOUT MENU
    elif scr_type == 'checkout':
        params_pkg = {
            '1': [],
            '2': [payment],
            '3': [cart],
            '4': [menu, Menu.display],
            'e': []
        }

        # "Pay with Cash"
        if user_input == '1':
            cash_tendered = input("How much do you pay me? ")
            payment.paid = cash_tendered
            cart.total = cart.cart_total(payment.paid)
            if cart.total == 0:
                menu.clear()
                print('You are all paid!')
                time.sleep(2)
            return 'checkout'

        # "Pay With Card"
        elif user_input == '2':
            while True:
                try:
                    full_name = input('First and Last name: ').split(' ')
                    payment.first_name = full_name[0]
                    payment.last_name = full_name[1]
                except IndexError:
                    print('Enter first and last name with a space in between.')
                    print('\n')
                except ValueError as e:
                    print(e)
                    continue
                try:
                    payment.cc_number = input('Credit card number: ')
                    payment.expiration = input('Expiration date [ MM/DD/YYYY ]: ')
                except ValueError as e:
                    print(e)
                    print('\n')
                    continue
                menu.clear()
                print('You are all paid!')
                time.sleep(2)
                break

        # "Sort the Cart"
        elif user_input == '3':
            sort_by = input('Sort by [ name ], [ subtotal ], or hit Enter to skip: ')
            params_pkg['3'].append(sort_by)
        elif user_input == '4':
            return 'main'
    return params_pkg


if __name__ == '__main__':
    main()