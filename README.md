# Grocery Store App
![jet engine shopping cart](http://i.dailymail.co.uk/i/pix/2013/07/23/article-2375282-1AF422F5000005DC-443_634x425.jpg)

## Context
This application was developed by [Mike Jolliffe](https://github.com/mike-jolliffe) in response to an engineering
interview exercise. It was built in `Python 3.6.2`.

All application source code and this readme can be found by clicking
[here](https://bitbucket.org/mike-jolliffe/grocerystoreapp/src/master/), or going to the following URL:

`https://bitbucket.org/mike-jolliffe/grocerystoreapp/src/master/`

I uploaded the project to BitBucket because I wasn't sure how to submit multiple files through the provided submission
page.

While the application uses only imports from the Standard Library and so should be simple to get up and running,
it is important to note that it will not work on older versions of Python. For example, string formatting
is handled with [f-strings](https://docs.python.org/3/whatsnew/3.6.html#whatsnew36-pep498), which are new in `3.6`. In addition, it uses new-style classes, which don't require
explicit inheritance from the base class `object` in the `Class` declaration process, but still deliver support for
descriptors such as `@staticmethod` and `@property` used in the program. In Python 2.X, explicitly inheriting from
`object` would be required to access them.

TL;DR

**_Please run this application in `Python 3.6`._**

The main program can be run by executing main.py with Python. For example:

```bash
 python3 main.py
```

Similarly, the test module for the `Cart` class can be run as follows:

```bash
 python3 test_cart.py
```

## Program Structure
### main.py
The `main.py` file is executed to run the application. It handles most of the control flow for the program, gathers user
input, and then packages it up and passes it to `menu.py` for execution.

### menu.py
The `menu.py` file utilizes a pair of dictionaries (one for the main menu, and one for the checkout menu)
as dispatchers to execute the appropriate functions based on commands
the user chose. `menu.py` also houses some administrative functions for screen clearing and pretty printing information.

### cart.py
The `cart.py` file contains the `Cart` class, which is a container for holding `GroceryProduct` objects (shopping items). In addition to managing
the inventory of items a user has added or removed during the shopping experience, it also tracks item subtotals and
the grand total cost of all items.

### item.py
The `item.py` file contains a `GroceryProduct` class, which is used to create items. Items are fairly "inert", only having
a single method that does anything beyond changing and displaying their own properties. It calculates the item subtotal.

### payment.py
The `payment.py` file stores payment type and amount. Much like `item.py` it does very little other than store and
report out on payment information.

### test_cart.py
The `test_cart.py` file implements unit tests for all methods within the Cart class. Ideally, I would write unit tests
for every class, but I started running out of weekend.

## Design Rationale
The majority of my design decisions were dictated by the assumption that this application's near-term next steps would be
the following: first, connecting to a database for getting/persisting transaction and item
information rather than just getting item descriptions from the user; and second, passing off the majority of the user
interaction to a client-side interface.

With these considerations in mind, here were some of my bigger design decisions:

* Try to keep all functionality that gathers user input in its own file along with most of the program flow. This makes
swapping out `main.py` for something like `main.js` (and handling input and control flow client-side) an easier task.

* Control command execution with a pair of dictionaries that act as dispatchers based on which menu a user is using.
I used the dictionary dispatcher approach because it makes adding/removing/filtering commands for given views very
simple. I chose to break commands across two dictionaries because I think they would likely service two different
views/routes in a web app (e.g., `/` and `/checkout`). This dictionary dispatcher approach is also a much safer option
than something like calling `eval()` on user commands.

* Don't add any additional properties to `GroceryProduct` objects. While this requirement wasn't explicitly stated, I left the shape
of GroceryProduct objects unmodified, choosing to connect a given item to its subtotal within the `Cart` class instead
of making the subtotal an additional `GroceryProduct` property. The requirements stated

    >"Grocery products have these attributes"

    then listed them, so I built those objects with nothing
more and nothing less. While it made a bit more work for me (as opposed to adding an `item_subtotal` to the `GroceryProduct`
object that's calculated on instantiation), I think it could certainly make interaction with a database easier. For example,
while the app currently gets all item information from the user, it would likely ultimately serve it from a database of some sort.
If a database has a Table like `dbo.GroceryProduct` with the exact fields described in the requirements, then keeping the
object the same makes database interaction simpler.

## Retrospective: Areas for Improvement ##

One major weakness in the design of my program is the two-mindedness of its error handling. I started out with the
thinking that I'd handle user input validation in-line as part of the program's control flow in `main.py`. Since a lot of
client-side libraries (and structuring as drop-downs, etc.) help with form/input validation, I figured my input-checking
logic could just be swapped out in the future.

However, as I built the program's various modules, I started thinking some of them could be
useful if re-purposed elsewhere (e.g., `Cart` is a great place to start in building something that stores geospatial objects
or photos for a carousel, etc.). I think it also makes testing easier and more systematic. That sort of functionality is
better served by modules that contain all their own error checking and don't **assume** valid inputs. Unfortunately, the
upshot is my approach changed mid-stream and I wound up with a program that does some error checking in `main`'s control
flow, and other error checking inside class methods themselves. In the case of `Cart`, some error-handling portions of the code
I built later are unreachable through main, because main handles those errors, so I have some redundant code.

I'm torn on this because putting a checker in each function (e.g., check input is `string`) is not very DRY compared to
many class methods sharing a `type_check()` function, but it helps ensure desired behavior at a very atomic level.

Ultimately, I think I wound up with a program that is the worst of both approaches: it introduces risk and headaches by
covering error handling sometimes here and sometimes there.

As mentioned, I would have ideally built all my unit tests (probably should have first) for the various modules, but I
was uncertain on the time-line for this problem.

## In Conclusion ##

I had a wonderful time working on this project!




